from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from lab_1.models import Friend
from .forms import FriendForm

# Create your views here.
# Implement index method like friend_list method in lab_1/views.py
@login_required(login_url='/admin/login')
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

# referenced from https://www.geeksforgeeks.org/django-modelform-create-form-from-models/
@login_required(login_url='/admin/login')
def add_friend(request):
    context ={}
  
    # create object of form
    form = FriendForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        return redirect("/lab-3")
        
    context['form']= form
    return render(request, "lab3_form.html", context)