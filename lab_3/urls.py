from django.urls import path
from .views import index, add_friend

urlpatterns = [
    # add index path
    path('', index, name='index'),
    # add add route
    path('add', add_friend, name='add_friend')
]