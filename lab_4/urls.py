from django.urls import path
from .views import index, add_note, note_list

urlpatterns = [
    # add index path
    path('', index, name='index'),
    # add add note path
    path('add', add_note, name='add_note'),
    # add note list path
    path('note-list', note_list, name="note_list")
]