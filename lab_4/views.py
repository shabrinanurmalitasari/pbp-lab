from django.shortcuts import render, redirect
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.
# index to render html response
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

# referenced from https://www.geeksforgeeks.org/django-modelform-create-form-from-models/
def add_note(request):
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if form.is_valid():
        # save the form data to model
        form.save()
        return redirect("/lab-4")
        
    context['form']= form
    return render(request, "lab4_form.html", context)

# render notes in card format
def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)