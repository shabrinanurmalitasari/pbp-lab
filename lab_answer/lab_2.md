1. Apakah perbedaan antara JSON dan XML?
JSON atau JavaScript Object Notation adalah format untuk pertukaran data yang ringan dan berdasarkan bahasa JavaScript.
XML atau Extensible Markup Language adalah bahasa markup yang mengubah dokumen menjadi human dan machine readable format.
Keduanya berperan sebagai syntax untuk penyimpanan/pertukaran data dalam web, perbedaan antara keduanya antara lain:
1. Data struktur JSON adalah map dengan key:value pairs dan XML adalah tree dengan tags.
2. JSON berdasarkan bahasa JavaScript dan XML berdasarkan SGML (Standrard Generalized Markup Language).
3. JSON lebih mudah untuk dipahami dibandingkan XML.
4. JSON less secured dibandingkan XML.
5. JSON lebih cepat untuk diproses dibandingkan XML.

Perbedaan lainnya dapat dilihat disini:
https://www.geeksforgeeks.org/difference-between-json-and-xml/

2. Apakah perbedaan antara HTML dan XML?
HTML adalah Hypertext Markup Language yang mendeskripsikan struktur suatu web page sedangkan XML adalah Extensible Markup Language yang mengubah format suatu data menjadi human dan machine readable format.
Perbedaan utama diantaranya adalah, HTML berfungsi untuk menampilkan data dan XML berfungsi sebagai syntax untuk menyimpan/mengirimkan data. Selain itu, tags dalam HTML adalah predefined tags sedangkan tags dalam XML terdefinisikan sesuai kebutuhan. 

Perbedaan lainnya dapat dilihat disini:
https://www.upgrad.com/blog/html-vs-xml/