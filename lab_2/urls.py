from django.urls import path
from .views import index, xml, json

urlpatterns = [
    # add index path
    path('', index, name='index'),
    # add xml path; import from views
    path('xml', xml),
    # add json path; import from views
    path('json', json)
]