from django.shortcuts import render
# import note model
from .models import Note
# import http response
from django.http.response import HttpResponse
# import serializers
from django.core import serializers

# Create your views here.
# index method to render HTML
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'index_lab2.html', response)

# xml method
def xml(request):
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")

# json method
def json(request):
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")