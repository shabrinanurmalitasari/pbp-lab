from django.contrib import admin

# Register your models here.
# Register Note model
from .models import Note
admin.site.register(Note)