from django.db import models

# Create your models here.
# Create Note model
class Note(models.Model):
    to = models.CharField(max_length=30)
    fr = models.CharField(max_length=30)
    title = models.CharField(max_length=100)
    message = models.TextField()
    def __str__(self) -> str:
        return f"{self.title} - {self.fr}"